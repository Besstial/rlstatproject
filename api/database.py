from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os

SQLALCHEMY_DATABASE_URL = "sqlite:///.sql_app.db"
SQLALCHEMY_DATABASE_URL = "sqlite:///db/.sql_app.db"
# SQLALCHEMY_DATABASE_URL = "postgresql://myuser:mypassword@127.0.0.1:5432/mydb"
# SQLALCHEMY_DATABASE_URL = os.getenv("DATABASE_URL")

engine_sqlite3 = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
engine_postgre = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine_sqlite3)

Base = declarative_base()