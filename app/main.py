import streamlit as st
import extra_streamlit_components as stx

from datetime import datetime, timedelta
import requests
import pandas as pd
import os

from dotenv import dotenv_values
config = {
    **dotenv_values(".env"),
    **os.environ
}

st.set_page_config(
    page_title="RLCStatistics",
    page_icon="1",
    layout="wide",
    initial_sidebar_state="expanded"
)

hide_streamlit_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """
st.markdown(hide_streamlit_style, unsafe_allow_html=True)

cookie_manager = stx.CookieManager()
if "token" not in st.session_state:
    st.session_state["token"] = cookie_manager.get("token")
if "login" not in st.session_state:
    st.session_state["login"] = requests.get(f"http://myapi:8000/status/", headers={'Authorization': f"{st.session_state['token']}"})
if "get_coins" not in st.session_state:
    st.session_state["get_coins"] = False

col_blank_1, col0, col_blanck_2 = st.columns(3)
with col0:
    st.title("RLCStatistics")
st.success(f"Data provided by Octane.gg")

if st.session_state["login"] is not True:
    login_form = st.sidebar.form('Login', clear_on_submit = True)
    login_form.subheader('Login')
    username = login_form.text_input('Username')
    password = login_form.text_input('Password', type='password')
    if login_form.form_submit_button('Login'):
        data = {"username" : username, "password" : password, "scope": "me"}
        result = requests.post(f"http://myapi:8000/token", data=data)
        token = result.json()
        if result.status_code == 200:
            st.session_state["token"] = f"{token['token_type']} {token['access_token']}"
            user = requests.get(f"http://myapi:8000/users/me/", headers={'Authorization': f"{st.session_state['token']}"}).json()
            st.session_state["login"] = True
            st.session_state["get_coins"] = user["get_today_coins"]
            cookie_manager.set("token", st.session_state["token"], expires_at=datetime.now()+timedelta(minutes=30))
            st.write(f"Welcome {username} ! You have {user['coins']} coins")
        else:
            st.sidebar.error(f"{token}")

if st.session_state["login"] is not True:
    register_form = st.sidebar.form('Register user', clear_on_submit = True)
    register_form.subheader('Register')
    username = register_form.text_input('Username')
    email = register_form.text_input('Email')
    password = register_form.text_input('Password', type='password')
    if register_form.form_submit_button('Register'):
        data={"username": username, "email": email, "hashed_password": password}
        result = requests.post(f"http://myapi:8000/users/", json=data)
        if result.status_code == 200:
            st.sidebar.success("Registered !")
        else:
            st.sidebar.error("Error during registration !")

if st.session_state["login"]:
    col1, col2, col3, col4 = st.columns(4)
    with col1:
        if st.button("Logout"):
            st.session_state["login"] = False
            cookie_manager.delete("token")
    with col2:
        if st.button("History"):
            list_bets = requests.get(f"http://myapi:8000/bet_history/", headers={'Authorization': f"{st.session_state['token']}"}).json()
            st.table(list_bets)
    with col3:
        if st.button("Rank"):
            list_rank = requests.get(f"http://myapi:8000/rank/", headers={'Authorization': f"{st.session_state['token']}"}).json()
            list_final = [{i["username"], i["coins"]} for i in list_rank]
            st.table(list_final)
    with col4:
        if not st.session_state["get_coins"]:
            if st.button("Get coins !"):
                amount_coins_now = requests.post(f"http://myapi:8000/users/me/refill/", headers={'Authorization': f"{st.session_state['token']}"}).json()
                st.write(f"You have now *{amount_coins_now['coins']}* coins !")
                st.session_state["get_coins"]=True

col_blank_1, col0 = st.columns(2)
with col_blank_1:
    st.write("**Upcoming matches**")
with col0:
    upcoming_matches = requests.get(f"http://myapi:8000/upcoming_matches/").json()
    st.table(upcoming_matches)

teams = requests.get(f"http://myapi:8000/get_teams").json()
try:
    indice_orange = teams.index(upcoming_matches[0]["orange_team"])
except:
    indice_orange = 64
try:
    indice_blue = teams.index(upcoming_matches[0]["blue_team"])
except:
    indice_blue = 82

col1, col2 = st.columns(2)
with col1: 
    o_name = st.selectbox("Orange team", teams, indice_orange)
with col2:
    b_name = st.selectbox("Blue team", teams, indice_blue)
result_api = requests.get(f"http://myapi:8000/{b_name}/{o_name}")
if result_api.status_code == 200:
    winner_team_api = result_api.json()
    string = f"""Winner : {winner_team_api["winner"]} with {int(winner_team_api["probas"]*100)}%"""
else:
    string = """No data"""
st.write(string)


infos_between_teams = requests.get(f"http://myapi:8000/get_infos/{o_name}/{b_name}").json()
with col1:
    st.json(infos_between_teams["orange_stats"])
with col2:
    st.json(infos_between_teams["blue_stats"])
st.dataframe(pd.read_json(infos_between_teams["history_matches"]))


st.header("Bet")
if len(upcoming_matches) == 0:
    st.write("No bet currently, see you next time !")
else:
    col1, col2, col3 = st.columns(3)
    with col1:
        choose_match = st.selectbox("Choose match", [i["id_match"] for i in upcoming_matches])
    with col2:
        choose_team = [[i["orange_team"], i["blue_team"]] for i in upcoming_matches if i["id_match"] == choose_match][0]
        team_bet = st.selectbox("Bet team", choose_team)
    with col3:
        amount_bet = st.number_input('Insert a amount', value=10, min_value=1, max_value=10000)
    st.write(f"The current bet is {team_bet} with {amount_bet}")
    bet_odds = requests.get(f"http://myapi:8000/get_odds/{choose_team[0]}/{choose_team[1]}")
    if bet_odds.status_code == 200:
        st.write(bet_odds.json())
    if bet_odds.status_code != 200:
        st.write("Odds at 2")
    if st.button("Bet !"):
        betting_request = requests.post(f"http://myapi:8000/do_bet/?id_match={choose_match}&team_name={team_bet}&amount={amount_bet}", headers={'Authorization': f"{st.session_state['token']}"})
        st.write(betting_request.json())

