from pydantic import BaseModel
from datetime import datetime

class UserBase(BaseModel):
    username: str
    email: str

class UserCreate(UserBase):
    hashed_password: str

class User(UserBase):
    id: int
    disabled: bool
    coins: float
    get_today_coins: bool

    class Config:
        orm_mode = True
    
class UserCoins(UserBase):
    coins: float


class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: str | None = None
    scopes: list[str] = []


class UpcomingMatch(BaseModel):
    id_match: str
    date: datetime
    orange_team: str
    blue_team: str
    to_display: bool

    class Config:
        orm_mode = True

class Match(BaseModel):
    id_match: str
    date: datetime
    orange_team: str
    blue_team: str
    orange_score: int
    blue_score: int
    winner: str
    
    class Config:
        orm_mode = True

class BetHistory(BaseModel):
    id: int
    id_match: str
    id_user: str
    date: datetime
    team_name: str
    amount: int

    class Config:
        orm_mode = True