from sqlalchemy.orm import Session
from sqlalchemy import asc, desc
import models, schemas
from datetime import datetime
import requests

from passlib.context import CryptContext
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def get_user(db: Session, username: int):
    return db.query(models.User).filter(models.User.username == username).first()

def get_user_by_id(db: Session, id_user: int):
    return db.query(models.User).filter(models.User.id == id_user).first()

def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()

def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password: str):
    return pwd_context.hash(password)

def create_user(db: Session, user: schemas.UserCreate):
    hashed_password = get_password_hash(user.hashed_password)
    db_user = models.User(username=user.username, email=user.email, hashed_password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def do_bet_user(db: Session, bet_amount: int, user: schemas.User):
    current_user = db.query(models.User).filter(models.User.username == user.username).first()
    if bet_amount > current_user.coins:
        take_amount = current_user.coins
    else:
        take_amount = bet_amount
    current_user.coins -= take_amount
    db.add(current_user)
    db.commit()
    db.refresh(current_user)
    return take_amount

def refill_coins_user(db: Session, user: schemas.User):
    current_user = db.query(models.User).filter(models.User.username == user.username).first()
    current_user.coins += 100
    current_user.get_today_coins = True
    db.add(current_user)
    db.commit()
    db.refresh(current_user)
    return current_user

def most_coins_users(db: Session):
    return db.query(models.User).order_by(desc(models.User.coins)).limit(10).all()

def get_upcoming_matches_db(db: Session):
    return db \
        .query(models.UpcomingMatchTable) \
        .filter(models.UpcomingMatchTable.to_display == True) \
        .filter(models.UpcomingMatchTable.date > datetime.now()) \
        .order_by(asc(models.UpcomingMatchTable.date)) \
        .limit(8).all()

def reset_get_coins_db(db: Session):
    users_update = db.query(models.User).filter(models.User.get_today_coins == True).all()
    for user in users_update:
        user.get_today_coins = False
        db.commit()

def get_user_history_bets(db: Session, user: schemas.User):
    return db \
        .query(models.BetHistory) \
        .filter(models.BetHistory.id_user == user.id) \
        .order_by(desc(models.BetHistory.date)) \
        .limit(10).all()

def get_bet(db: Session, id_user : int, id_match: str):
    return db.query(models.BetHistory).filter(models.BetHistory.id_match == id_match).filter(models.BetHistory.id_user == id_user).first()

def create_bet(db: Session, user: schemas.User, id_match: str, team_name: str, amount: int):
    already_exist = get_bet(db=db, id_user=user.id, id_match=id_match)
    if already_exist == None:
        matche = db.query(models.UpcomingMatchTable).filter(models.UpcomingMatchTable.id_match == id_match).first()
        bethistoryexample = models.BetHistory(id_match=id_match, id_user=user.id, date=matche.date, team_name=team_name, amount=amount)
        db.add(bethistoryexample)
        db.commit()
        db.refresh(bethistoryexample)
        return bethistoryexample
    else:
        already_exist.amount += amount
        db.commit()
        return already_exist


def add_upcoming_match(db: Session, upcoming_match: schemas.UpcomingMatch):
    already_exist = get_upcoming_match_db(db=db, id_match=upcoming_match.id_match)
    if already_exist == None:
        db_upcoming_match = models.UpcomingMatchTable(
            id_match = upcoming_match.id_match,
            date = upcoming_match.date,
            orange_team = upcoming_match.orange_team,
            blue_team = upcoming_match.blue_team,
            to_display = upcoming_match.to_display
        )
        db.add(db_upcoming_match)
        db.commit()
        db.refresh(db_upcoming_match)
    else:
        already_exist.date = upcoming_match.date
        already_exist.orange_team = upcoming_match.orange_team
        already_exist.blue_team = upcoming_match.blue_team
        already_exist.to_display = upcoming_match.to_display
        db.commit()
    return upcoming_match

def get_upcoming_matches_played_db(db: Session, time: datetime):
    return db.query(models.UpcomingMatchTable).filter(models.UpcomingMatchTable.date < time).all()

def get_match_db(db: Session, id_match):
    return db.query(models.MatchTable).filter(models.MatchTable.id_match == id_match).first()

def add_played_match_db(db: Session, match: schemas.Match):
    already_exist = get_match_db(db=db, id_match=match.id_match)
    if already_exist == None:
        db_match = models.MatchTable(
            id_match = match.id_match,
            date = match.date,
            orange_team = match.orange_team,
            blue_team = match.blue_team,
            orange_score = match.orange_score,
            blue_score = match.blue_score,
            winner = match.winner
        )
        db.add(db_match)
        db.commit()
        db.refresh(db_match)

def delete_upcoming_match_played(db: Session, id_match):
    db.query(models.UpcomingMatchTable).filter(models.UpcomingMatchTable.id_match == id_match).delete()
    db.commit()

def get_upcoming_match_db(db: Session, id_match: str):
    return db.query(models.UpcomingMatchTable).filter(models.UpcomingMatchTable.id_match == id_match).first()

def get_good_bets(db: Session, id_match: str, team_name: str):
    return db \
        .query(models.BetHistory) \
        .filter(models.BetHistory.id_match == id_match) \
        .filter(models.BetHistory.team_name == team_name) \
        .all()

def update_bet_coins_winner_users(db: Session, id_match: str, team_winner: str, odd: float):
    list_bet_winners = get_good_bets(db=db, id_match=id_match, team_name=team_winner)
    for winner in list_bet_winners:
        user = get_user_by_id(db=db , id_user=winner.id_user)
        user.coins += (odd * winner.amount)
        db.commit()

def process_match_when_finish_db(db: Session):
    now=datetime.today()
    perhaps_played_sql = get_upcoming_matches_played_db(db=db, time=now)
    for match in perhaps_played_sql:
        match_infos = requests.get(f"https://zsr.octane.gg/matches/{match.id_match}").json()
        try:
            orange_score = match_infos["orange"]["score"]
        except KeyError:
            orange_score = 0
        try:
            blue_score = match_infos["blue"]["score"]
        except KeyError:
            blue_score = 0
        if (blue_score==0) and (orange_score==0):
            pass
        else:
            winner = "B" if blue_score > orange_score else "O"
            team_winner = match.blue_team if blue_score > orange_score else match.orange_team
            MatchExample = schemas.Match(id_match=match.id_match, date=match.date, orange_team=match.orange_team, blue_team=match.blue_team, orange_score=orange_score, blue_score=blue_score, winner=winner)
            add_played_match_db(db=db, match=MatchExample)
            delete_upcoming_match_played(db=db, id_match=match.id_match)
            update_bet_coins_winner_users(db=db, id_match=match.id_match, team_winner=team_winner, odd=2)

def update_upcoming_matches_db(db: Session, upcoming_matches: dict):
    for id_match, upcoming_match in upcoming_matches.items():
        if (upcoming_match["orange_team"] != "TBD") & (upcoming_match["blue_team"] != "TBD"):
            to_display = True
        else:
            to_display = False
        UpcomingMatchExample = schemas.UpcomingMatch(id_match=id_match, date=upcoming_match["date"], orange_team=upcoming_match["orange_team"], blue_team=upcoming_match["blue_team"], to_display=to_display)
        add_upcoming_match(db=db, upcoming_match=UpcomingMatchExample)

def get_upcoming_matches_2(dict_upcoming_matches={}):
    dict_upcoming_matches={}
    today=datetime.today().strftime('%Y-%m-%d')
    url=f"https://zsr.octane.gg/matches?mode=3&tier=S&tier=A&region=NA&region=EU&region=INT&after={today}"
    json_results = requests.get(url).json()
    for i in json_results["matches"]:
        try:
            orange_name = i["orange"]["team"]["team"]["name"]
        except KeyError:
            orange_name = "TBD"
        try:
            blue_name = i["blue"]["team"]["team"]["name"]
        except KeyError:
            blue_name = "TBD"
        dict_upcoming_matches[i["_id"]] = {"date":datetime.strptime(i["date"], '%Y-%m-%dT%H:%M:%SZ'), "orange_team":orange_name, "blue_team":blue_name}
    return dict_upcoming_matches