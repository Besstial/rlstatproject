data "aws_availability_zones" "available" {}

resource "aws_iam_role" "ec2_adrien" {
  name = "ec2_adrien"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
      tag-key = "tag-value"
  }
}

resource "aws_iam_role_policy" "policy_for_ec2_role" {
  name = "role_policy"
  role = "${aws_iam_role.ec2_adrien.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "profile_ec2" {
  name = "profile_ec2"
  role = "${aws_iam_role.ec2_adrien.name}"
}

resource "aws_instance" "instance" {
    ami = "ami-0a351d518d9b16d34"       
    instance_type = "t2.micro"
    security_groups = ["${aws_security_group.ec2-security-group.name}"]
    tags = {
        Name = "Web server by TerraForm"
    }
    user_data = file("launch_config_user_data.sh")

  iam_instance_profile = "${aws_iam_instance_profile.profile_ec2.name}"
}


resource "aws_security_group" "ec2-security-group" {
  name = "ec2_security_group_adrien"
  description = "Security group for deploying scaling instances"

  ingress {
    from_port = "22"
    to_port = "22"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = "80"
    to_port = "80"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


/*
resource "aws_launch_configuration" "aws-launch-config" {
  name_prefix   = "${var.application_name}_launch_config"
  image_id      = "${data.aws_ami.linux-ecs-optimized.id}"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.ecs-security-group.id}"]
  key_name = "${var.key_name}"
  iam_instance_profile = "${aws_iam_instance_profile.ec2-instance-profile.name}"

  # Add Storage
  root_block_device {
    volume_type                 = "gp2"
    volume_size                 = 8
    iops                        = 100
    delete_on_termination       = "true"
  }

  ebs_block_device {
    device_name                 = "/dev/xvdcz"
    volume_type                 = "gp2"
    volume_size                 = 22
    iops                        = 100
  }

  lifecycle {
    create_before_destroy = true
  }
  user_data = "${file("launch_config_user_data.sh")}"
}
*/