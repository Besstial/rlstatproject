variable "region" {
    description = "The AWS region to create resource in."
    default = "eu-west-3"
}   