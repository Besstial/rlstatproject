from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, DateTime
from sqlalchemy.orm import relationship

from database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    coins = Column(Float, default=100)
    get_today_coins = Column(Boolean, default=False)
    disabled = Column(Boolean, default=False)

class UpcomingMatchTable(Base):
    __tablename__ = "upcoming_matches"
    id_match = Column(String, primary_key=True, index=True)
    date = Column(DateTime, index=True)
    orange_team = Column(String, index=True)
    blue_team = Column(String, index=True)
    to_display = Column(Boolean, default=False)

class MatchTable(Base):
    __tablename__ = "matches"
    id_match = Column(String, primary_key=True, index=True)
    date = Column(String, index=True)
    orange_team = Column(String, index=True)
    blue_team = Column(String, index=True)
    orange_score = Column(Integer, index=True)
    blue_score = Column(Integer, index=True)
    winner = Column(String, index=True)

class BetHistory(Base):
    __tablename__ = "bet_history"
    id = Column(Integer, primary_key=True, index=True)
    id_match = Column(String, ForeignKey("upcoming_matches.id_match"), index=True)
    id_user = Column(String, ForeignKey("users.id"), index=True)
    date = Column(DateTime, index=True)
    team_name = Column(String, index=True)
    amount = Column(Integer, index=True)