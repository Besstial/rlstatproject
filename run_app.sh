#!/bin/bash

sudo apt install python3-pip -y
sudo apt install uvicorn -y
pip3 install virtualenv

ssh -i "ec2_keys.pem" ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com

cd ~/rlstatproject/
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt

export ROOTPATHPROJECT="/home/ubuntu/rlstatproject"

ps -aux | grep uvicorn | awk '{print $2}' | head -1
kill

nohup uvicorn api.main:app &
sudo systemctl start nginx.service
nohup streamlit run $ROOTPATHPROJECT/app/main.py &