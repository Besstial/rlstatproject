from datetime import datetime, timedelta
import pandas as pd
import pickle
import numpy as np

from fastapi import Depends, FastAPI, HTTPException, Security, status
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
from fastapi.middleware.cors import CORSMiddleware
from jose import JWTError, jwt
from pydantic import ValidationError

import os
from dotenv import dotenv_values
config = {
    **dotenv_values(".env"),
    **os.environ
}

import warnings
warnings.simplefilter("ignore")

from sqlalchemy.orm import Session
import crud, models, schemas
from database import SessionLocal, engine_sqlite3
models.Base.metadata.create_all(bind=engine_sqlite3)

from apscheduler.schedulers.background import BackgroundScheduler

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="token",
    scopes={"me": "Read information about the current user."},
)

api = FastAPI()
origins = ["*"]
api.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


scheduler = BackgroundScheduler()

def tasks_daily(db=next(get_db())):
    crud.reset_get_coins_db(db=db)

def do_process_matches(db=next(get_db())):
    upcoming_matches = crud.get_upcoming_matches_2()
    crud.update_upcoming_matches_db(db=db, upcoming_matches=upcoming_matches)
    crud.process_match_when_finish_db(db=db)

scheduler.add_job(do_process_matches, 'interval', minutes=5, start_date=datetime.now() + timedelta(seconds=8))
scheduler.add_job(tasks_daily, 'interval', days=1, start_date='2023-01-18 00:00:00')
scheduler.start()


def authenticate_user(db, username: str, password: str):
    user = crud.get_user(db, username)
    if not user:
        return False
    if not crud.verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = "Bearer"
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": authenticate_value},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_scopes = payload.get("scopes", [])
        token_data = schemas.TokenData(scopes=token_scopes, username=username)
    except (JWTError, ValidationError):
        raise credentials_exception
    user = crud.get_user(db, username=token_data.username)
    if user is None:
        raise credentials_exception
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not enough permissions",
                headers={"WWW-Authenticate": authenticate_value},
            )
    return user


async def get_current_active_user(current_user: schemas.User = Security(get_current_user, scopes=["me"])):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@api.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)

@api.post("/token", response_model=schemas.Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username, "scopes": form_data.scopes},
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "bearer"}


@api.get("/users/me/", response_model=schemas.User)
async def read_users_me(current_user: schemas.User = Depends(get_current_active_user)):
    return current_user

@api.get("/status/")
async def read_system_status(current_user: schemas.User = Depends(get_current_active_user)):
    return True

@api.post("/users/me/refill/", response_model=schemas.User)
async def do_bet_me(current_user: schemas.User = Depends(get_current_active_user), db: Session = Depends(get_db)):
    result = crud.refill_coins_user(db, current_user)
    return result

@api.get("/upcoming_matches/", response_model=list[schemas.UpcomingMatch])
def get_upcoming_matches(db: Session = Depends(get_db)):
    return crud.get_upcoming_matches_db(db)

@api.get("/rank/", response_model=list[schemas.User])
def get_rank_users(db: Session = Depends(get_db)):
    return crud.most_coins_users(db)

@api.get("/bet_history/", response_model=list[schemas.BetHistory])
def get_user_bets(current_user: schemas.User = Depends(get_current_active_user), db: Session = Depends(get_db)):
    return crud.get_user_history_bets(db=db, user=current_user)

@api.post("/do_bet/", response_model=schemas.BetHistory)
def do_bet_user(id_match: str, team_name: str, amount: int, current_user: schemas.User = Depends(get_current_active_user), db: Session = Depends(get_db)):
    real_amount_bet = crud.do_bet_user(db, amount, current_user)
    return crud.create_bet(db=db, user=current_user, id_match=id_match, team_name=team_name, amount=real_amount_bet)


def create_columns(color):
    columns = [
        f'{color}team_stats_demo_inflicted_mean',
        f'{color}team_stats_core_assists_mean',
        f'{color}team_stats_core_goals_mean',
        f'{color}team_stats_core_saves_mean',
        f'{color}team_stats_core_score_mean',
        f'{color}team_stats_core_shots_mean',
        f'{color}team_stats_core_shootingPercentage'
    ]
    return columns

team_columns = create_columns(color="")
blue_team_columns = create_columns(color="blue_")
orange_team_columns = create_columns(color="orange_")


def get_data_team_for_prediction(team_name, dataframe):
    filter_orange_name = dataframe["orange_team_team_name"] == team_name
    filter_blue_name = dataframe["blue_team_team_name"] == team_name
    df_orange = dataframe[filter_orange_name][orange_team_columns + ["date"]]
    df_blue = dataframe[filter_blue_name][blue_team_columns + ["date"]]
    df_orange.columns = team_columns + ["date"]
    df_blue.columns = team_columns + ["date"]
    df_result = pd.concat([df_orange, df_blue], ignore_index=True).sort_values(by="date", ascending=False)
    df_result = df_result.drop("date", axis=1)[:5]
    return df_result.mean().tolist()

def do_prediction(team_name_1, team_name_2, clf, dataframe):
    team_1 = get_data_team_for_prediction(team_name=team_name_1, dataframe=dataframe)
    team_2 = get_data_team_for_prediction(team_name=team_name_2, dataframe=dataframe)
    for_predictions = [team_1 + team_2]
    result = clf.predict(for_predictions)[0]
    probas = clf.predict_proba(for_predictions)[0]
    if result == 1:
        return team_name_1, probas[1]
    else:
        return team_name_2, probas[0]

def two_sides_prediction(team_name_1, team_name_2, clf, dataframe):
    result_side_1=do_prediction(team_name_1=team_name_1, team_name_2=team_name_2, clf=clf, dataframe=dataframe)
    result_side_2=do_prediction(team_name_1=team_name_2, team_name_2=team_name_1, clf=clf, dataframe=dataframe)
    if result_side_1[0]==result_side_2[0]:
        return (result_side_1[0], round((result_side_1[1]+result_side_2[1])/2, 2))
    team_1_victory_probas = round((result_side_1[1] + (1-result_side_2[1]))/2, 2)
    team_2_victory_probas = round((result_side_2[1] + (1-result_side_1[1]))/2, 2)
    if team_1_victory_probas >= team_2_victory_probas:
        return (result_side_1[0], team_1_victory_probas)
    if team_1_victory_probas < team_2_victory_probas:
        return (result_side_2[0], team_2_victory_probas)

def two_sides_prediction(team_name_1, team_name_2, clf, dataframe):
    result_side_1=do_prediction(team_name_1=team_name_1, team_name_2=team_name_2, clf=clf, dataframe=dataframe)
    result_side_2=do_prediction(team_name_1=team_name_2, team_name_2=team_name_1, clf=clf, dataframe=dataframe)
    if result_side_1[0]==result_side_2[0]:
        return (result_side_1[0], round((result_side_1[1]+result_side_2[1])/2, 2))
    team_1_victory_probas = round((result_side_1[1] + (1-result_side_2[1]))/2, 2)
    team_2_victory_probas = round((result_side_2[1] + (1-result_side_1[1]))/2, 2)
    if team_1_victory_probas >= team_2_victory_probas:
        return (result_side_1[0], team_1_victory_probas)
    if team_1_victory_probas < team_2_victory_probas:
        return (result_side_2[0], team_2_victory_probas)

def get_stat_team(team_name, dataframe):
    filter_orange_name = dataframe["orange_team_team_name"] == team_name
    filter_blue_name = dataframe["blue_team_team_name"] == team_name
    df_orange = dataframe[filter_orange_name][orange_team_columns + ["date"]]
    df_blue = dataframe[filter_blue_name][blue_team_columns + ["date"]]
    df_orange.columns = team_columns + ["date"]
    df_blue.columns = team_columns + ["date"]
    df_result = pd.concat([df_orange, df_blue], ignore_index=True).sort_values(by="date", ascending=False)
    df_result = df_result.drop("date", axis=1)[:5]
    return df_result.mean().to_json()

df = pd.read_csv(f"{config['ROOTPATHPROJECT']}/.data/matches_2.csv")

def get_list_teams(dataframe):
    orange_teams = dataframe["orange_team_team_name"].unique()
    blue_teams = dataframe["blue_team_team_name"].unique()
    return np.unique(np.concatenate((orange_teams, blue_teams), axis=None)).tolist()

def get_all_matchs_between_teams(team_name_1, team_name_2, dataframe):
    dataframe_filter = dataframe[((dataframe["orange_team_team_name"] == team_name_1) & (dataframe["blue_team_team_name"] == team_name_2)) | ((dataframe["orange_team_team_name"] == team_name_2) & (dataframe["blue_team_team_name"] == team_name_1))]
    dataframe_result = dataframe_filter[["date", "event_name", "event_tier", "stage_name", "orange_team_team_name", "orange_score", "blue_score", "blue_team_team_name"]]
    return  dataframe_result

def get_odds(value):
    if value < 0.5 or value > 1:
        raise ValueError
    if value > 0.75:
        value = 0.75
    winner_odd, looser_odd = round(1/value, 2), round(1/(1-value), 2)
    return winner_odd, looser_odd

def get_bet_odds(team_name_1, team_name_2, clf, dataframe):
    winner_result = two_sides_prediction(team_name_1=team_name_1, team_name_2=team_name_2, clf=clf, dataframe=dataframe)
    winner_name, percent_victory = winner_result[0], winner_result[1]
    winner_odd, looser_odd = get_odds(value=percent_victory)
    if winner_name == team_name_1:
        return {team_name_1 : winner_odd, team_name_2 : looser_odd}
    return {team_name_1 : looser_odd, team_name_2 : winner_odd}

teams = get_list_teams(dataframe=df)

clf = pickle.load(open(f"{config['ROOTPATHPROJECT']}/.data/model.pkl", "rb"))



@api.get("/{b_name}/{o_name}")
def predict(b_name: str, o_name: str):
    winner_team = two_sides_prediction(team_name_1=b_name, team_name_2=o_name, clf=clf, dataframe=df)
    return {"winner" : winner_team[0], "probas" : winner_team[1]}

@api.get("/stats_team/{team_name}")
def get_stat(team_name: str):
    print(team_name)
    return get_stat_team(team_name=team_name, dataframe=df).tolist()

@api.get("/get_teams")
def get_teams():
    return teams

@api.get("/get_infos/{orange_team}/{blue_team}")
def get_infos(orange_team: str, blue_team: str):
    orange_team_stat = get_stat_team(team_name=orange_team, dataframe=df)
    blue_team_stat = get_stat_team(team_name=blue_team, dataframe=df)
    winner_team = two_sides_prediction(team_name_1=orange_team, team_name_2=blue_team, clf=clf, dataframe=df)
    matches = get_all_matchs_between_teams(team_name_1=orange_team, team_name_2=blue_team, dataframe=df).to_json()
    result = {
        "orange_stats" : orange_team_stat,
        "blue_stats" : blue_team_stat, 
        "history_matches" : matches,
        "winner" : winner_team
    }
    return result

@api.get("/get_odds/{b_name}/{o_name}")
def get_bet(b_name: str, o_name: str):
    return get_bet_odds(team_name_1=o_name, team_name_2=b_name, clf=clf, dataframe=df)
