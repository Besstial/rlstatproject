#ssh -i ec2_keys.pem ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com rm -r /home/ubuntu/rlstatproject/
#ssh -i ec2_keys.pem ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com mkdir -p /home/ubuntu/rlstatproject/
scp -i ec2_keys.pem -r ./rlstatproject/app/ ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/
scp -i ec2_keys.pem -r ./rlstatproject/.data/ ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/
scp -i ec2_keys.pem -r ./rlstatproject/api/ ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/
scp -i ec2_keys.pem -r ./rlstatproject/.env ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/
scp -i ec2_keys.pem -r ./rlstatproject/requirements.txt ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/
scp -i ec2_keys.pem -r ./rlstatproject/run_app.sh ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/

scp -i ec2_keys.pem -r ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/rlstatproject/.sql_app.db ./rlstatproject/
scp -i ec2_keys.pem -r ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:/etc/nginx ./rlstatproject



scp -i ec2_keys.pem -r ./rlstatproject ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/

# new way
rsync -avL --progress -e "ssh -i /home/besstial/ec2_keys.pem" --exclude='.*' ./rlstatproject ubuntu@ec2-3-87-201-240.compute-1.amazonaws.com:~/